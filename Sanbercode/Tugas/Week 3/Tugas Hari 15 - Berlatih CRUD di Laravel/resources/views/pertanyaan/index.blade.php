@extends('layout.master')

@section('content')
    <div class="ml-2 mt-3 mr-2">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Pertanyaan</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th style="width: 10px">#</th>
                            <th>Judul</th>
                            <th>Isi</th>
                            <th style="width: 40px">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($pertanyaan as $key => $post)
                            <tr>
                                <td>{{ $key + 1 }}</td>
                                <td>{{ $post->judul }}</td>
                                <td>{{ $post->isi }}</td>
                                <td style="display: flex">
                                    <a href="/pertanyaan/{{$post->id}}" class="btn btn-info btn-sm">Lihat</a>
                                    <a href="/pertanyaan/{{$post->id}}/edit" class="btn btn-default btn-sm">Ubah</a>
                                    <form action="/pertanyaan/{{$post->id}}" method="post">
                                        @csrf
                                        @method('DELETE')
                                        <input type="submit" value="Hapus" class="btn btn-danger btn-sm">
                                    </form>
                                </td>
                            </tr>
                            @empty
                                <tr>
                                    <td colspan="4" align="center"> Tidak ada Pertanyaan.</td>
                                </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
            <!-- /.card-body -->
        </div>
    </div>
    
@endsection