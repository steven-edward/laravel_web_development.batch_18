@extends('layout.master')

@section('content')
    <div class="ml-2 mt-3 mr-2">
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">Buat Pertanyaan</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form role="form" action="/pertanyaan/{{$pertanyaan->id}}" method="POST">
                @csrf
                @method('PUT')
                <div class="card-body">
                    <div class="form-group">
                        <label for="judul">Judul</label>
                        <input type="text" class="form-control" id="judul" name="judul" value="{{old ('judul',$pertanyaan->judul)}}" placeholder="Masukkan Judul">
                        @error('judul')
                            <div class="alert alert-danger">{{$message}}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="isi">Isi</label>
                        <textarea class="form-control" rows="5" id="isi" name="isi" placeholder="Pertanyaan">{{old ('isi',$pertanyaan->isi)}}</textarea>
                        @error('isi')
                            <div class="alert alert-danger">{{$message}}</div>
                        @enderror
                    </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Perbaharui</button>
                </div>
            </form>
        </div>
    </div>
@endsection