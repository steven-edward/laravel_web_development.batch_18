@extends('layout.master')

@section('content')
<div class="ml-2 mt-3 mr-2">
        <div class="card">
            <div class="card-header">
                <h2 class="card-title">{{$pertanyaan->judul}}</h2>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <table>
                    <tbody>
                        <tr>
                            <th>{{$pertanyaan->isi}}</th>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection