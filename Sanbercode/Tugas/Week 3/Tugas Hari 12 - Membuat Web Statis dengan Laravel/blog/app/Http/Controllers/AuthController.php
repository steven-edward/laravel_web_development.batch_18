<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register()
    {
        return view('register');
    }

    public function kirim(Request $request)
    {
        $f_name = $request->first_name;
        $l_name = $request->last_name;
        return view('welcome',compact('f_name', 'l_name'));
    }

    public function welcome (){
        return view('index');
    }
}
