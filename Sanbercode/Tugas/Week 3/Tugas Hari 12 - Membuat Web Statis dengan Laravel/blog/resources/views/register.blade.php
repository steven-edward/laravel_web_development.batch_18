<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
</head>

<body>
    <form action="/kirim" method="POST">
        @csrf
        <h1>Buat Account Baru!</h1>
        <h2>Sign Up Form</h2>

        <!-- First Name -->
        <label for="first_name">First Name :</label><br>
        <input type="text" id="first_name" name="first_name"><br>
        <br>

        <!-- Last Name -->
        <label for="last_name">Last Name :</label><br>
        <input type="text" id="last_name" name="last_name"><br>
        <br>

        <!-- Gender -->
        <label for="gender">Gender</label><br>
        <input type="radio" name="gender" value="0">Male <br>
        <input type="radio" name="gender" value="1">Female <br>
        <input type="radio" name="gender" value="3">Other <br>
        <br>

        <!-- Nationality -->
        <label for="nationality">Nationality :</label><br>
        <select name="Nationality" id="nationality">
            <option value="1">Indonesian</option>
            <option value="0">Singaporean</option>
            <option value="2">Malaysian</option>
            <option value="3">Australian</option>
        </select><br>
        <br>

        <!-- Language -->
        <label for="language">Language spoken :</label><br>
        <input type="checkbox" name="language" value="0">Bahasa Indonesia <br>
        <input type="checkbox" name="language" value="1">English <br>
        <input type="checkbox" name="language" value="2">Other <br>
        <br>

        <!-- Bio -->
        <label for="bio">Bio :</label><br>
        <textarea name="Bio" id="bio" cols="30" rows="10"></textarea><br>
        <br>

        <input type="submit" value="Sign Up">
    </form>
</body>

</html>